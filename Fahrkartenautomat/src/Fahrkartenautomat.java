﻿import java.util.Scanner;

//7Durch diesen Schritt können wir das Schreiben vieler Variablen verkürzen.
// Außerdem können wir jetzt viele Kaufoptionen einfach, ohne Komplikationen und
// ohne Wiederholung der Deklaration der Variablen hinzufügen und ändern.

// Der Unterschied zwischen dem neuen und dem alten Programm besteht
// darin, dass das neue Programm flexibler geworden ist, aktualisiert und geändert werden
// kann und interaktiver mit Kunden ist.

class Fahrkartenautomat {
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		int wahlkarte;
		double zuZahlenderBetrag;
		double Preis = 0;
		
		double [] FahrkartenPrise = { 2.90,3.30,3.60,1.90,8.60,9.00,9.60 
				,23.50 ,24.30,24.90};
		
		
		String [] Fahrkarten = {"(1)     Einzelfahrschein Berlin AB",
								"(2)     Einzelfahrschein Berlin BC	",
								"(3)     Einzelfahrschein Berlin ABC	",
								"(4)     Kurzstrecke",
								"(5)     Tageskarte Berlin AB",
								"(6)     Tageskarte Berlin BC",
								"(7)     Tageskarte Berlin ABC",
								"(8)     Kleingruppen-Tageskarte Berlin AB",
								"(9)     Kleingruppen-Tageskarte Berlin BC",
								"(10)    Kleingruppen-Tageskarte Berlin ABC"};
		
		
		for(int i=0; i< Fahrkarten.length; i++) {
			int Wahl = i + 1 ;
			System.out.println(Fahrkarten[i] + "     Die Preis ist: " + FahrkartenPrise [i] );
			System.out.println();
		}
		
System.out.println("Ihre Wahl");
wahlkarte = tastatur.nextInt();
while (wahlkarte > 10 || wahlkarte <1) {
	System.out.println("Ihre Wahl ist falsch bitte Wählen Sie noch einmal eine richtige Auswahl");
	wahlkarte = tastatur.nextInt();
}
	if (wahlkarte == 1) {Preis = FahrkartenPrise[0];}
	else if (wahlkarte == 2) {Preis = FahrkartenPrise[1];}
	else if (wahlkarte == 3) {Preis = FahrkartenPrise[2];}
	else if (wahlkarte == 4) {Preis = FahrkartenPrise[3];}
	else if (wahlkarte == 5) {Preis = FahrkartenPrise[4];}
	else if (wahlkarte == 6) {Preis = FahrkartenPrise[5];}
	else if (wahlkarte == 7) {Preis = FahrkartenPrise[6];}
	else if (wahlkarte == 8) {Preis = FahrkartenPrise[7];}
	else if (wahlkarte == 9) {Preis = FahrkartenPrise[8];}
	else if (wahlkarte == 10) {Preis = FahrkartenPrise[9];}

	


System.out.print("Bitte Anzahl der Fahrkarten eingeben: ");
int anzahlFahrkarten = tastatur.nextInt();
if(anzahlFahrkarten < 1 || anzahlFahrkarten > 10) {
	anzahlFahrkarten = 1;
	System.out.println("Falsche Anzahl ausgewählt, es wird nur eine Fahrkarte gekauft.");
}




zuZahlenderBetrag = Preis * anzahlFahrkarten;

return zuZahlenderBetrag;
	}
	

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		// Geldeinwurf
		// -----------
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
				// -------------------------------

				if (rückgabebetrag > 0.0) {
					System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
					System.out.println("wird in folgenden Münzen ausgezahlt:");

					while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
					{
						System.out.println("2 EURO");
						rückgabebetrag -= 2.0;
					}
					while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
					{
						System.out.println("1 EURO");
						rückgabebetrag -= 1.0;
					}
					while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
					{
						System.out.println("50 CENT");
						rückgabebetrag -= 0.5;
					}
					while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
					{
						System.out.println("20 CENT");
						rückgabebetrag -= 0.2;
					}
					while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
					{
						System.out.println("10 CENT");
						rückgabebetrag -= 0.1;
					}
					while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
					{
						System.out.println("5 CENT");
						rückgabebetrag -= 0.05;
					}
				}
	}

	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rückgabebetrag;
		for (int i = 1 ;; i++) {
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückgabebetrag);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");}
	}

}