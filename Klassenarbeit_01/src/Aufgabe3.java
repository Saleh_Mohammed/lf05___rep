//Fehlersuche
// @version 3.0 vom 01.12.2020
//@author Tenbusch

import java.util.Scanner;

public class Aufgabe3 {
    
	public static void main(String[] args) {
	}
    	double Umfangberechnung = umfangsberechnung();
    
    
    public static String umfangsberechnung(double durchmesser,double umfang) {  //die Zeile ist korrekt, Finger weg!
    	
    	//Variablendeklaration
    	Scanner scan = new Scanner(System.in);
    	final double PI = 3.141;
    	String Überschrift = "Umfangsberechnung eines eckigen Kreises";
        
    	//Eingabe
    	System.out.println(Überschrift);
    	System.out.println("Bitte geben Sie den Durchmesser ein: ");
    	durchmesser = scan.nextDouble();
    	
    	//Verarbeitung
    	umfang = PI * durchmesser;
        
    	//Ausgabe
    	System.out.println("Der Umfang beträgt " + umfang);
    	
    	//Automatisierte Auswertung  ------ab hier ist alles korrekt, nichts verändern------
    	return "A2: " + Überschrift + ";" + PI + "; Durchmesser: " + durchmesser + "; Umfang: " + umfang;
    }
    
} 
